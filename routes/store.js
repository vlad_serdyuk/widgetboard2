var express = require('express');
var router = express.Router();

var swig  = require('swig');
var React = require('react');
var ReactDOM = require('react-dom/server');
var ReactRouter = require('react-router');
var reactRoutes = require('../app/routes');

router.get('/google-maps', function(req, res, next) {
    matchRoute(req, res, next);
});

router.get('/yandex-maps', function(req, res, next) {
  matchRoute(req, res, next);
});

var matchRoute = function(req, res, next){
  ReactRouter.match({ routes: reactRoutes.default, location: req.url }, function(err, redirectLocation, renderProps) {
    if (err) {
      res.status(500).send(err.message)
    } else if (redirectLocation) {
      res.status(302).redirect(redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps) {
      var html = ReactDOM.renderToString(React.createElement(ReactRouter.RoutingContext, renderProps));
      res.status(200).send(html);
    } else {
      res.status(404).send('Page Not Found')
    }
  });
}

module.exports = router;
