import alt from '../alt';
import SettingsPanelActions from '../actions/SettingsPanelActions';

class BoardStore {
  constructor() {
     this.bindActions(SettingsPanelActions);
     this.activeWidget = null;
  }

  onGetWidgetFormServerSuccess(data) {
    this.activeWidget = data;
  }
}

export default alt.createStore(BoardStore);
