import alt from '../alt';
import SettingsPanelActions from '../actions/SettingsPanelActions';
import BoardStore from '../stores/BoardStore';

class SettingsPanelStore {
  constructor() {
     this.bindActions(SettingsPanelActions);
     this.activeWidget = null;
  }

  onGetWidgetFormServerSuccess(data) {
    this.activeWidget = data;
  }

  onGetWidgetFormServerFail(jqXhr) {
    console.log(jqXhr.responseJSON && jqXhr.responseJSON.message || jqXhr.responseText || jqXhr.statusText);
  }
}

export default alt.createStore(SettingsPanelStore);
