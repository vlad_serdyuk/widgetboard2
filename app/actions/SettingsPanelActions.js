import alt from '../alt.js'

class SettingsPanelActions {
   constructor() {
     this.generateActions(
       'getWidgetFormServerSuccess',
       'getWidgetFormServerFail'
     );
   }

  getWidgetFormServer(widgetName) {
    $.ajax({ url: '/store/' + widgetName })
      .done((data) => {
        this.actions.getWidgetFormServerSuccess(data)
      })
      .fail((jqXhr) => {
        this.actions.getWidgetFormServerFail(jqXhr)
      });
  }
}

export default alt.createActions(SettingsPanelActions);
