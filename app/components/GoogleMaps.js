import React from 'react';
import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";

class GoogleMaps extends React.Component {
  render () {
    return (
      <section style={{height: "100%"}}>
        <GoogleMap defaultZoom={3} defaultCenter={{lat: -25.363882, lng: 131.044922}} />
      </section>
    )
  }
  // <GoogleMapLoader containerElement={ <div  style={{height: "100%"}}/> }
  //   googleMapElement={
  //     <GoogleMap
  //       defaultZoom={3}
  //       defaultCenter={{lat: -25.363882, lng: 131.044922}}
  //       onClick={this.handleMapClick}>
  //       {this.state.markers.map((marker, index) => {
  //         return (
  //           <Marker
  //             {...marker}
  //             onRightclick={this.handleMarkerRightclick.bind(this, index)} />
  //         );
  //       })}
  //     </GoogleMap>
  //   }
  // />
}

export default GoogleMaps;
