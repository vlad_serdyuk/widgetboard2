import React from 'react';
import {Link} from 'react-router';
import BoardStore from '../stores/BoardStore'
import BoardActions from '../actions/BoardActions';

class Board extends React.Component {
  constructor() {
    super();
    this.state = BoardStore.getState();
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    BoardStore.listen(this.onChange);
  }

  componentWillUnmount() {
    BoardStore.unlisten(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

  render () {
    return (
      <div id='viewer'>
        <div className='inner'>
          <div onClick={this.props.onToggleClick} className='toggle'>
          </div>
        </div>
        <div dangerouslySetInnerHTML={{__html: this.state.activeWidget ? this.state.activeWidget : '<p>empty</p>'}}>
        </div>
      </div>
    )
  }
}

export default Board;
