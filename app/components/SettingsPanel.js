import React from 'react';
import {Link} from 'react-router';
import SettingsPanelStore from '../stores/SettingsPanelStore'
import SettingsPanelActions from '../actions/SettingsPanelActions';

class SettingsPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = SettingsPanelStore.getState();
    this.onWidgetSelect = this.onWidgetSelect.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    SettingsPanelStore.listen(this.onChange);
  }

  componentWillUnmount() {
     SettingsPanelStore.unlisten(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

  onWidgetSelect(e) {
    SettingsPanelActions.getWidgetFormServer('google-maps');
  }

  render () {
    return (
      <div id='main'>
        <header id='header'>Store</header>
        <span onClick={this.onWidgetSelect}>google-maps</span>
        <div onClick={this.props.onToggleClick} className='toggle'>
        </div>
      </div>
    )
  }
}

export default SettingsPanel;
