import React from 'react';
import SettingsPanel from './SettingsPanel';
import Board from './Board';

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  handleToggleClick() {
    let body = $('body');
    body.hasClass('fullscreen') ? body.removeClass('fullscreen') : body.addClass('fullscreen');
  }

  render() {
    return (
      <div>
        <SettingsPanel onToggleClick={this.handleToggleClick} />
        <Board onToggleClick={this.handleToggleClick} />
      </div>
    );
  }
}

export default Home;
