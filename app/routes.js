import React from 'react';
import {Route} from 'react-router';
import App from './components/App';
import Home from './components/Home';
import GoogleMaps from './components/GoogleMaps';
import YandexMaps from './components/YandexMaps';

export default (
  <Route component={App}>
   <Route path='/' component={Home} />
   <Route path='/google-maps' component={GoogleMaps} />
   <Route path='/yandex-maps' component={YandexMaps} />
 </Route>
);
